import React, { Component } from "react";
import "./UserList.scss";
import classNames from "classnames";
import { connect } from "react-redux";
import { fetch_data_one_chat_room_by_chat_room_id } from "../../actions/ChatRoomPage";
import GlobalChannel from "../../actioncable/global_channel";
import { createConsumerWithToken } from "../../actioncable/createConsumerWithToken";
class UserList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentIndex: 0,
			flash: false
		};
	}
	componentDidMount() {
		createConsumerWithToken(localStorage.getItem("token"));
		GlobalChannel.subscribe(this.props.userId, this.props.dispatch);
	}
	render() {
		const { chatRoomUserWithVendors, Notify } = this.props;
		console.log(Notify);
		var id_notify = Notify.content.chatRoomId;
		if (chatRoomUserWithVendors && chatRoomUserWithVendors.length > 0) {
			var chatRoomUserWithVendor = chatRoomUserWithVendors.map(
				(chatRoomUserWithVendor, index) => {
					return (
						<div
							onClick={async () => {
								await this.setState({
									currentIndex: index
								});
								this.props.fetchDataOneChatRoomByChatRoomId(
									chatRoomUserWithVendor.id
								);
							}}
							key={index}
							className={classNames(
								"list-group-item list-group-item-action online",
								{
									active: this.state.currentIndex == index
								}
							)}
						>
							<img
								src={window.dig(chatRoomUserWithVendor, [
									"user",
									"photoUserAvatar",
									"photoSmUrl"
								])}
								className="d-block ui-w-40 rounded-circle"
								alt="ok done"
							/>
							<div className="media-body ml-3">
								{window.dig(chatRoomUserWithVendor, ["user", "name"])}
								<div className="chat-status small">
									<span className="badge badge-dot" />
									&nbsp; Online
								</div>
							</div>
							{id_notify ? (
								<div className="badge badge-outline-success">New</div>
							) : (
								""
							)}
						</div>
					);
				}
			);
		}

		return (
			<div className="chat-sidebox col">
				{/* Chat contacts header */}
				<div className="flex-grow-0 px-4">
					<div className="media align-items-center">
						<div className="media-body">
							<input
								type="text"
								className="form-control chat-search my-3"
								placeholder="Search..."
							/>
							<div className="clearfix" />
						</div>
						<a
							href="javascript:void(0)"
							className="chat-sidebox-toggler d-lg-none d-block text-muted text-large font-weight-light pl-3"
						>
							×
						</a>
					</div>
					<hr className="border-light m-0" />
				</div>

				<div>
					<div className="chat-contacts list-group" id="style-1">
						{/* Online */}

						{chatRoomUserWithVendor}
					</div>
					{/* / .chat-contacts */}
				</div>
			</div>
		);
	}
}

export default connect(
	state => ({
		userId: state.auth.userId,
		Notify: state.Notify
	}),

	dispatch => ({
		dispatch,
		fetchDataOneChatRoomByChatRoomId: chatRoomId => {
			dispatch(fetch_data_one_chat_room_by_chat_room_id(chatRoomId));
		}
	})
)(UserList);

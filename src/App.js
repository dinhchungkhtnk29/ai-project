import React from "react";
import "./App.scss";
import { Switch } from "react-router-dom";
import { Route, BrowserRouter } from "react-router-dom";
import routes from "./routes/routes";
import Layout from "./Layout/Layout";

function App(props) {
  return (
    <BrowserRouter>
      <div className="layout-wrapper layout-2">
        <div className="layout-inner" style={{ minHeight: "auto" }}>
          <Layout>{showContent(routes)}</Layout>
        </div>
      </div>
    </BrowserRouter>
  );
}

function showContent(routes) {
  let result = null;
  if (routes.length > 0) {
    result = routes.map((route, index) => {
      return <Route key={index} path={route.path} exact={route.exact} component={route.content} />;
    });
  }

  return <Switch>{result}</Switch>;
}

export default App;

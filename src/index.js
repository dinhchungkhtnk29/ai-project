import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'

import './index.scss'
import 'bootstrap/dist/css/bootstrap.css'
import './scss/font-awesome/css/all.css'

import App from './App'
import * as serviceWorker from './serviceWorker'

import { createStore, compose, applyMiddleware } from 'redux'
import appReducer from './reducers'
import { Provider } from 'react-redux'

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(appReducer, composeEnhancer(applyMiddleware(thunk)))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()

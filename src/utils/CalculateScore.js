const calculateScore = (boxChoosen, direction, boxes) => {
  let score;
  if (isNeedToRaiQuan(boxChoosen, boxes)) {
    let newBoxes;
    if (boxes[0].stoneNumber || boxes[0].quanNumber || boxes[6].stoneNumber || boxes[6].quanNumber) {
      if (boxChoosen > 1 && boxChoosen < 7) {
        newBoxes = boxes.map(box => {
          if (box.id > 1 && box.id < 7) {
            return {
              ...box,
              stoneNumber: 1
            };
          }
          return box;
        });
      } else {
        newBoxes = boxes.map(box => {
          if (box.id > 7) {
            return {
              ...box,
              stoneNumber: 1
            };
          }
          return box;
        });
      }
    } else {
      newBoxes = JSON.parse(JSON.stringify(boxes));
    }
    let stopBox = [0];
    divideStone(boxChoosen, direction, newBoxes, stopBox);
    score = collectStone(stopBox[0], direction, newBoxes);
    return {
      score: score,
      boxes: newBoxes
    };
  } else {
    let newBoxes = JSON.parse(JSON.stringify(boxes));
    let stopBox = [0];
    divideStone(boxChoosen, direction, newBoxes, stopBox);
    score = collectStone(stopBox[0], direction, newBoxes);
    return {
      score: score,
      boxes: newBoxes
    };
  }
};

const isNeedToRaiQuan = (boxChoosen, boxes) => {
  if (boxChoosen > 1 && boxChoosen < 7) {
    let isNeed = true;
    boxes.forEach(box => {
      if (box.id > 1 && box.id < 7) {
        if (box.stoneNumber) {
          isNeed = false;
        }
      }
    });
    return isNeed;
  }
  if (boxChoosen > 7) {
    let isNeed = true;
    boxes.forEach(box => {
      if (box.id > 7) {
        if (box.stoneNumber) {
          isNeed = false;
        }
      }
    });
    return isNeed;
  }
};

const divideStone = (boxChoosen, direction, boxes, stopBox) => {
  let numberToDivide = boxes[boxChoosen - 1].stoneNumber;
  let newBoxes = boxes;
  let newStopBox = stopBox;
  newBoxes[boxChoosen - 1].stoneNumber = 0;
  if (direction === "CLOCKWISE") {
    let nextBoxIndex = boxChoosen - 1;
    for (let i = 0; i < numberToDivide; i++) {
      if (nextBoxIndex + 1 === 12) {
        nextBoxIndex = 0;
      } else {
        nextBoxIndex = nextBoxIndex + 1;
      }
      newBoxes[nextBoxIndex].stoneNumber = newBoxes[nextBoxIndex].stoneNumber + 1;
    }
    newStopBox[0] = nextBoxIndex + 1;
    let nextBoxIndexCheck;
    if (nextBoxIndex + 1 === 12) {
      nextBoxIndexCheck = 0;
    } else {
      nextBoxIndexCheck = nextBoxIndex + 1;
    }
    if (nextBoxIndexCheck !== 0 && nextBoxIndexCheck !== 6) {
      if (newBoxes[nextBoxIndexCheck].stoneNumber) {
        divideStone(nextBoxIndexCheck + 1, "CLOCKWISE", newBoxes, stopBox);
      }
    }
  }
  if (direction === "COUNTER_CLOCKWISE") {
    let nextBoxIndex = boxChoosen - 1;
    for (let i = 0; i < numberToDivide; i++) {
      if (nextBoxIndex - 1 === -1) {
        nextBoxIndex = 11;
      } else {
        nextBoxIndex = nextBoxIndex - 1;
      }
      newBoxes[nextBoxIndex].stoneNumber = newBoxes[nextBoxIndex].stoneNumber + 1;
    }
    newStopBox[0] = nextBoxIndex + 1;
    let nextBoxIndexCheck;
    if (nextBoxIndex - 1 === -1) {
      nextBoxIndexCheck = 11;
    } else {
      nextBoxIndexCheck = nextBoxIndex - 1;
    }
    if (nextBoxIndexCheck !== 0 && nextBoxIndexCheck !== 6) {
      if (newBoxes[nextBoxIndexCheck].stoneNumber) {
        divideStone(nextBoxIndexCheck + 1, "COUNTER_CLOCKWISE", newBoxes, stopBox);
      }
    }
  }
};

const collectStone = (stopBox, direction, boxes) => {
  let score = 0;
  if (direction === "CLOCKWISE") {
    if (boxes[stopBox - 1].stoneNumber !== 0) {
      let nextBoxIndex, nextBoxIndexCollect;
      for (let i = 0; i < 5; i++) {
        if (stopBox === 12) {
          nextBoxIndex = 0;
        } else {
          nextBoxIndex = stopBox - 1 + 1;
        }
        if (stopBox === 11) {
          nextBoxIndexCollect = 0;
        } else if (stopBox === 12) {
          nextBoxIndexCollect = 1;
        } else {
          nextBoxIndexCollect = stopBox - 1 + 2;
        }
        if (
          !boxes[nextBoxIndex].stoneNumber &&
          !boxes[nextBoxIndex].quanNumber &&
          boxes[nextBoxIndexCollect].stoneNumber
        ) {
          if (nextBoxIndexCollect === 0 || nextBoxIndexCollect === 6) {
            if (
              (boxes[nextBoxIndexCollect].stoneNumber >= 5 && boxes[nextBoxIndexCollect].quanNumber) ||
              boxes[nextBoxIndexCollect].quanNumber === 0
            ) {
              score = score + boxes[nextBoxIndexCollect].stoneNumber + 5 * boxes[nextBoxIndexCollect].quanNumber;
              boxes[nextBoxIndexCollect].stoneNumber = 0;
            } else {
              break;
            }
          } else {
            score = score + boxes[nextBoxIndexCollect].stoneNumber;
            boxes[nextBoxIndexCollect].stoneNumber = 0;
          }
        } else {
          break;
        }
        stopBox = nextBoxIndexCollect + 1;
      }
    } else {
      score = 0;
    }
    return score;
  } else {
    if (boxes[stopBox - 1].stoneNumber) {
      let nextBoxIndex, nextBoxIndexCollect;
      for (let i = 0; i < 5; i++) {
        if (stopBox === 1) {
          nextBoxIndex = 11;
        } else {
          nextBoxIndex = stopBox - 1 - 1;
        }
        if (stopBox === 1) {
          nextBoxIndexCollect = 10;
        } else if (stopBox === 2) {
          nextBoxIndexCollect = 11;
        } else {
          nextBoxIndexCollect = stopBox - 1 - 2;
        }
        if (
          !boxes[nextBoxIndex].stoneNumber &&
          !boxes[nextBoxIndex].quanNumber &&
          boxes[nextBoxIndexCollect].stoneNumber
        ) {
          if (nextBoxIndexCollect === 0 || nextBoxIndexCollect === 6) {
            if (
              (boxes[nextBoxIndexCollect].stoneNumber >= 5 && boxes[nextBoxIndexCollect].quanNumber) ||
              boxes[nextBoxIndexCollect].quanNumber === 0
            ) {
              score = score + boxes[nextBoxIndexCollect].stoneNumber + 5 * boxes[nextBoxIndexCollect].quanNumber;
              boxes[nextBoxIndexCollect].stoneNumber = 0;
            } else {
              break;
            }
          } else {
            score = score + boxes[nextBoxIndexCollect].stoneNumber;
            boxes[nextBoxIndexCollect].stoneNumber = 0;
          }
        } else {
          break;
        }
        stopBox = nextBoxIndexCollect + 1;
      }
    } else {
      score = 0;
    }
    return score;
  }
};

export default calculateScore;

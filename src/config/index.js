const Config = {
  BASE_URL: "https://7edfc5a0c362c990b2-staging.hanpu.vn",
  WS_URL: "wss://7edfc5a0c362c990b2-staging.hanpu.vn/cable/",
  ID_ANALYTICS: "UA-153404265-1"
};

export default Config;

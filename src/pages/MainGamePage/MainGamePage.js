import React from 'react'
import MainGame from '../../components/MainGame/MainGame'

class MainGamePage extends React.Component {
  render() {
    return (
      <div>
        <MainGame></MainGame>
      </div>
    )
  }
}

export default MainGamePage

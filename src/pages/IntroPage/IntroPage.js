import React, { Component } from "react";
import intro from "../../img/intro.jpg";
import "./IntroPage.scss";
import { Link } from "react-router-dom";

export default class IntroPage extends Component {
  render() {
    return (
      <div className="intro_page" style={{ backgroundImage: `url(${intro})` }}>
        <div className="group_button">
          <Link to="/main-game/player">
            <div>
              <button>Player chơi trước</button>
            </div>
          </Link>
          <Link to="/main-game/bot">
            <div>
              <button>Bot chơi trước</button>
            </div>
          </Link>
          <Link to="/main-game/player-2">
            <div>
              <button>Player chơi trước C2</button>
            </div>
          </Link>
          <Link to="/main-game/bot-2">
            <div>
              <button>Bot chơi trước C2</button>
            </div>
          </Link>
        </div>
      </div>
    );
  }
}

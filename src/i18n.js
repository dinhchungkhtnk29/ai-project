import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import sidebar_vn from './translate/vn/sidebar'
import sidebar_en from './translate/en/sidebar'
import phanquyen_vn from './translate/vn/phanquyen'
import phanquyen_en from './translate/en/phanquyen'
import header_vn from './translate/vn/header'
import header_en from './translate/en/header'
import detector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'
i18n
  .use(detector)
  .use(initReactI18next)
  .init({
    resources: {
      'vi-VN': {
        sidebar: sidebar_vn,
        phanquyen: phanquyen_vn,
        header: header_vn
      },
      vn: {
        sidebar: sidebar_vn,
        phanquyen: phanquyen_vn,
        header: header_vn
      },
      en: {
        sidebar: sidebar_en,
        phanquyen: phanquyen_en,
        header: header_en
      },
      lng: 'vn',

      fallbackLng: 'vn',

      // have a common namespace used around the full app
      ns: ['ns1', 'ns2'],
      defaultNS: 'ns1',

      keySeparator: false, // we use content as keys

      interpolation: {
        escapeValue: false, // not needed for react!!
        formatSeparator: ','
      },

      react: {
        wait: true
      }
    }
  })

export default i18n

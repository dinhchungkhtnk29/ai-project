import { combineReducers } from 'redux';
import boxes from './boxes';
import boxesBot from './boxesBot';

const appReducer = combineReducers({
  boxes: boxes,
  boxesBot: boxesBot
});

export default appReducer;

const initState = {
  boxesInfo: [
    {
      id: 1,
      score: 5,
      stoneNumber: 0,
      quanNumber: 1
    },
    {
      id: 2,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 3,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 4,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 5,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 6,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 7,
      score: 5,
      stoneNumber: 0,
      quanNumber: 1
    },
    {
      id: 8,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 9,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 10,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 11,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    },
    {
      id: 12,
      score: 5,
      stoneNumber: 5,
      quanNumber: 0
    }
  ],
  boxChosen: '',
  player: {
    score: 0,
    stoneNumber: 0,
    quanNumber: 0
  },
  stoneNumberNeedToDivide: 0,
  boxCurrentStopId: '',
  stoneNumberNeedToDivideCounter: 0,
  scorePlayer1: 0,
  scorePlayer2: 0,
  boxCurrentCollectId: 0,
  collectDirection: '',
  turnPlayer: 2,
  botCalculate: 0,
  endGame: false
};

const isNeedToRaiQuan = (player, boxes) => {
  if (player === 1) {
    let isNeed = true;
    boxes.forEach(box => {
      if (box.id > 1 && box.id < 7) {
        if (box.stoneNumber) {
          isNeed = false;
        }
      }
    });
    return isNeed;
  }
  if (player === 2) {
    let isNeed = true;
    boxes.forEach(box => {
      if (box.id > 7) {
        if (box.stoneNumber) {
          isNeed = false;
        }
      }
    });
    return isNeed;
  }
};

const divideClockwise = (boxesInfo, boxChosen) => {
  let numberStoneToDivine;
  let tempBoxToCompare = boxChosen;
  let stopBox;
  boxesInfo.forEach(box => {
    if (box.id === tempBoxToCompare) {
      numberStoneToDivine = box.stoneNumber;
    }
  });
  let newBoxesInfo = boxesInfo.map(box => {
    if (box.id === tempBoxToCompare) {
      return {
        ...box,
        stoneNumber: 0
      };
    }
    if (box.id > tempBoxToCompare && numberStoneToDivine) {
      numberStoneToDivine = numberStoneToDivine - 1;
      stopBox = box.id;
      return {
        ...box,
        stoneNumber: box.stoneNumber + 1
      };
    }
    return box;
  });
  while (numberStoneToDivine) {
    newBoxesInfo = newBoxesInfo.map(box => {
      if (box.id >= 0 && numberStoneToDivine) {
        numberStoneToDivine = numberStoneToDivine - 1;
        stopBox = box.id;
        return {
          ...box,
          stoneNumber: box.stoneNumber + 1
        };
      }
      return box;
    });
  }
  if (stopBox !== 6 && stopBox !== 12) {
    if (newBoxesInfo[stopBox].stoneNumber) {
      return divideClockwise(newBoxesInfo, newBoxesInfo[stopBox].id);
    }
  }
  return {
    boxesInfo: newBoxesInfo,
    stopBox: stopBox
  };
};

const boxes = (state = initState, action) => {
  switch (action.type) {
    case 'CHANGE_BOX_CHOSE':
      return {
        ...state,
        boxChosen: action.data,
        boxCurrentStopId: action.data
      };
    case 'DIVIDE_CLOCKWISE': {
      let newBoxesInfo = state.boxesInfo.map(box => {
        if (box.id === state.boxChosen) {
          return {
            ...box,
            stoneNumber: 0
          };
        }
        return box;
      });
      return {
        ...state,
        boxesInfo: [...newBoxesInfo],
        stoneNumberNeedToDivide: action.data
      };
    }
    case 'STEP_DIVIDE_CLOCKWISE': {
      let boxCurrentStopId;
      if (state.boxCurrentStopId + 1 > 12) {
        boxCurrentStopId = 1;
      } else {
        boxCurrentStopId = state.boxCurrentStopId + 1;
      }
      let newBoxesInfo = state.boxesInfo.map(box => {
        if (box.id === boxCurrentStopId) {
          return {
            ...box,
            stoneNumber: box.stoneNumber + 1
          };
        }
        return box;
      });
      return {
        ...state,
        boxesInfo: [...newBoxesInfo],
        stoneNumberNeedToDivide: state.stoneNumberNeedToDivide - 1,
        boxCurrentStopId: boxCurrentStopId
      };
    }
    case 'COLLECT_SCORE': {
      let score;
      let newBoxesInfo = state.boxesInfo.map(box => {
        if (action.data.stopBox === box.id) {
          score = box.stoneNumber + box.quanNumber * 5;
          return {
            ...box,
            stoneNumber: 0,
            quanNumber: 0
          };
        }
        return box;
      });
      if (action.data.player === 1) {
        return {
          ...state,
          boxesInfo: [...newBoxesInfo],
          boxCurrentStopId: action.data.stopBox,
          boxCurrentCollectId: state.boxCurrentCollectId + 1,
          scorePlayer1: state.scorePlayer1 + score,
          collectDirection: action.data.direction
        };
      }
      return {
        ...state,
        boxesInfo: [...newBoxesInfo],
        boxCurrentStopId: action.data.stopBox,
        boxCurrentCollectId: state.boxCurrentCollectId + 1,
        scorePlayer2: state.scorePlayer2 + score,
        collectDirection: action.data.direction
      };
    }
    case 'DIVIDE_COUNTER_CLOCKWISE': {
      let newBoxesInfo = state.boxesInfo.map(box => {
        if (box.id === state.boxChosen) {
          return {
            ...box,
            stoneNumber: 0
          };
        }
        return box;
      });
      return {
        ...state,
        boxesInfo: [...newBoxesInfo],
        stoneNumberNeedToDivideCounter: action.data
      };
    }
    case 'STEP_DIVIDE_COUNTER_CLOCKWISE': {
      let boxCurrentStopId;
      if (state.boxCurrentStopId - 1 < 1) {
        boxCurrentStopId = 12;
      } else {
        boxCurrentStopId = state.boxCurrentStopId - 1;
      }
      let newBoxesInfo = state.boxesInfo.map(box => {
        if (box.id === boxCurrentStopId) {
          return {
            ...box,
            stoneNumber: box.stoneNumber + 1
          };
        }
        return box;
      });
      return {
        ...state,
        boxesInfo: [...newBoxesInfo],
        stoneNumberNeedToDivideCounter: state.stoneNumberNeedToDivideCounter - 1,
        boxCurrentStopId: boxCurrentStopId
      };
    }
    case 'CHECK_RAI_QUAN': {
      if (isNeedToRaiQuan(action.data, state.boxesInfo)) {
        if (action.data === 1) {
          let newBoxesInfo = state.boxesInfo.map(box => {
            if (box.id > 1 && box.id < 7) {
              return {
                ...box,
                stoneNumber: 1
              };
            }
            return box;
          });
          return {
            ...state,
            boxesInfo: [...newBoxesInfo],
            scorePlayer1: state.scorePlayer1 - 5,
            botCalculate: state.botCalculate + 1
          };
        } else {
          let newBoxesInfo = state.boxesInfo.map(box => {
            if (box.id > 7) {
              return {
                ...box,
                stoneNumber: 1
              };
            }
            return box;
          });
          return {
            ...state,
            boxesInfo: [...newBoxesInfo],
            scorePlayer2: state.scorePlayer2 - 5,
            botCalculate: state.botCalculate + 1
          };
        }
      }
      return {
        ...state,
        botCalculate: state.botCalculate + 1
      };
    }
    case 'END_GAME': {
      let remainScorePlayer1 = 0,
        remainScorePlay2 = 0;
      state.boxesInfo.forEach(box => {
        if (box.id > 1 && box.id < 7) {
          remainScorePlayer1 = remainScorePlayer1 + box.stoneNumber;
        } else if (box.id > 7) {
          remainScorePlay2 = remainScorePlay2 + box.stoneNumber;
        }
      });
      let newBoxesInfo = state.boxesInfo.map(box => {
        return {
          ...box,
          stoneNumber: 0
        };
      });
      return {
        ...state,
        boxesInfo: [...newBoxesInfo],
        scorePlayer1: state.scorePlayer1 + remainScorePlayer1,
        scorePlayer2: state.scorePlayer2 + remainScorePlay2,
        endGame: true
      };
    }
    case 'CHANGE_TURN_PLAYER': {
      return {
        ...state,
        turnPlayer: action.data
      };
    }
    // case 'CHANGE_TURN_PLAYER_BOT':
    //   return {
    //     ...state,
    //     botFirst: false,
    //     turnPlayer: action.data
    //   };
    case 'RESET_BOXES':
      return initState;
    default:
      return state;
  }
};

export default boxes;

import React from "react";
import MainGamePage from "../pages/MainGamePage/MainGamePage";
import IntroPage from "../pages/IntroPage/IntroPage";
import MainGameBot from "../components/MainGame/MainGameBot";
import EndGamePage from "../pages/EndGamePage/EndGamePage";
import MainGameBotC2 from "../components/MainGame/MainGameBotC2";
import MainGameC2 from "../components/MainGame/MainGameC2";

const routes = [
  {
    path: "/main-game/player",
    exact: true,
    content: () => <MainGamePage></MainGamePage>
  },
  {
    path: "/main-game/bot",
    exact: true,
    content: () => <MainGameBot></MainGameBot>
  },
  {
    path: "/main-game/player-2",
    exact: true,
    content: () => <MainGameC2></MainGameC2>
  },
  {
    path: "/main-game/bot-2",
    exact: true,
    content: () => <MainGameBotC2></MainGameBotC2>
  },
  {
    path: "/main-game/end-game",
    exact: true,
    content: () => <EndGamePage></EndGamePage>
  },
  {
    path: "/",
    exact: true,
    content: () => <IntroPage></IntroPage>
  }
];
export default routes;

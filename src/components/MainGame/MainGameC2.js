import React, { Component } from "react";
import background from "../../img/banco2.png";
import bigBackground from "../../img/background.jpg";
import "./MainGame.scss";
import Box from "./Box/Box";
import { connect } from "react-redux";
import classnames from "classnames";
import calculateScore from "../../utils/CalculateScore";
import { Link } from "react-router-dom";

class MainGameBot extends Component {
  state = {
    boxes: [],
    playerActive: 2,
    endGame: false,
    frameCounter: 500,
    playerWin: "",
    arrayScoreBotCalculate: [],
    arrayScoreBotCalculateTurnEnermy: [],
    arrayRiskBotCalculate: [],
    arrayTotalScoreAndRiskBotCalculate: [],
    arrayScoreBotCalculateTurn2: [],
    arrayScoreBotCalculateTurnEnermyTurn2: [],
    arrayRiskBotCalculateTurn2: [],
    arrayTotalScoreAndRiskBotCalculateTurn2: [],
    botThinking: false
  };

  async componentDidMount() {
    this.formatBoxes(this.props.boxes);
  }

  componentWillUnmount() {
    this.props.resetBoxes();
    this.setState({
      playerWin: "",
      endGame: false,
      arrayScoreBotCalculate: []
    });
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.boxes !== nextProps.boxes) {
      this.formatBoxes(nextProps.boxes);
    }
    //clockwise
    if (nextProps.stoneNumberNeedToDivide !== this.props.stoneNumberNeedToDivide && nextProps.stoneNumberNeedToDivide) {
      let timeout = undefined;
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        nextProps.stepDivideClockwise();
      }, this.state.frameCounter);
    }
    //clockwise continues
    if (
      nextProps.stoneNumberNeedToDivide !== this.props.stoneNumberNeedToDivide &&
      nextProps.stoneNumberNeedToDivide === 0
    ) {
      if (nextProps.boxCurrentStopId !== 6 && nextProps.boxCurrentStopId !== 12) {
        let nextBoxCanBeCollect;
        if (nextProps.boxCurrentStopId + 1 === 12) {
          nextBoxCanBeCollect = 0;
        } else if (nextProps.boxCurrentStopId + 1 === 13) {
          nextBoxCanBeCollect = 1;
        } else {
          nextBoxCanBeCollect = nextProps.boxCurrentStopId + 1;
        }
        if (nextProps.boxes[nextProps.boxCurrentStopId].stoneNumber) {
          nextProps.changeBoxChose(nextProps.boxCurrentStopId + 1);
          let timeout = undefined;
          clearTimeout(timeout);
          timeout = setTimeout(() => {
            nextProps.divideClockwise(nextProps.boxes[nextProps.boxCurrentStopId].stoneNumber);
          }, this.state.frameCounter);
        } else if (
          !nextProps.boxes[nextProps.boxCurrentStopId].stoneNumber &&
          nextProps.boxes[nextBoxCanBeCollect].stoneNumber
        ) {
          //colect
          if (nextBoxCanBeCollect === 0 || nextBoxCanBeCollect === 6) {
            if (
              (nextProps.boxes[nextBoxCanBeCollect].stoneNumber >= 5 &&
                nextProps.boxes[nextBoxCanBeCollect].quanNumber) ||
              nextProps.boxes[nextBoxCanBeCollect].quanNumber === 0
            ) {
              let newBoxCurrentStop = nextBoxCanBeCollect + 1;
              let timeout = undefined;
              clearTimeout(timeout);
              timeout = setTimeout(() => {
                nextProps.collectScore(newBoxCurrentStop, this.state.playerActive, "CLOCKWISE");
              }, this.state.frameCounter);
            } else {
              this.changePlayerActive();
            }
          } else {
            let newBoxCurrentStop = nextBoxCanBeCollect + 1;
            let timeout = undefined;
            clearTimeout(timeout);
            timeout = setTimeout(() => {
              nextProps.collectScore(newBoxCurrentStop, this.state.playerActive, "CLOCKWISE");
            }, this.state.frameCounter);
          }
        } else {
          this.changePlayerActive();
        }
      } else {
        if (
          nextProps.boxCurrentStopId === 6 &&
          nextProps.boxes[6].stoneNumber === 0 &&
          nextProps.boxes[6].quanNumber === 0 &&
          nextProps.boxes[7].stoneNumber
        ) {
          let timeout = undefined;
          clearTimeout(timeout);
          timeout = setTimeout(() => {
            nextProps.collectScore(8, this.state.playerActive, "CLOCKWISE");
          }, this.state.frameCounter);
        } else if (
          nextProps.boxCurrentStopId === 12 &&
          nextProps.boxes[0].stoneNumber === 0 &&
          nextProps.boxes[0].quanNumber === 0 &&
          nextProps.boxes[1].stoneNumber
        ) {
          let timeout = undefined;
          clearTimeout(timeout);
          timeout = setTimeout(() => {
            nextProps.collectScore(2, this.state.playerActive, "CLOCKWISE");
          }, this.state.frameCounter);
        } else {
          this.changePlayerActive();
        }
      }
    }
    //counter_clockwise
    if (
      nextProps.stoneNumberNeedToDivideCounter !== this.props.stoneNumberNeedToDivideCounter &&
      nextProps.stoneNumberNeedToDivideCounter
    ) {
      let timeout = undefined;
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        nextProps.stepDivideCounterClockwise();
      }, this.state.frameCounter);
    }
    //counter_clockwise continues
    if (
      nextProps.stoneNumberNeedToDivideCounter !== this.props.stoneNumberNeedToDivideCounter &&
      nextProps.stoneNumberNeedToDivideCounter === 0
    ) {
      if (nextProps.boxCurrentStopId !== 2 && nextProps.boxCurrentStopId !== 8) {
        let prevBoxCheckIndex, prevBoxToCollect;
        if (nextProps.boxCurrentStopId - 2 < 0) {
          prevBoxCheckIndex = 11;
        } else {
          prevBoxCheckIndex = nextProps.boxCurrentStopId - 2;
        }
        if (nextProps.boxCurrentStopId === 2) {
          prevBoxToCollect = 11;
        } else if (nextProps.boxCurrentStopId === 1) {
          prevBoxToCollect = 10;
        } else {
          prevBoxToCollect = nextProps.boxCurrentStopId - 3;
        }
        if (nextProps.boxes[prevBoxCheckIndex].stoneNumber) {
          let chooseBoxPrev;
          if (nextProps.boxCurrentStopId - 1 < 1) {
            chooseBoxPrev = 12;
          } else {
            chooseBoxPrev = nextProps.boxCurrentStopId - 1;
          }
          nextProps.changeBoxChose(chooseBoxPrev);
          let timeout = undefined;
          clearTimeout(timeout);
          timeout = setTimeout(() => {
            nextProps.divideCounterClockwise(nextProps.boxes[prevBoxCheckIndex].stoneNumber);
          }, this.state.frameCounter);
        } else if (!nextProps.boxes[prevBoxCheckIndex].stoneNumber && nextProps.boxes[prevBoxToCollect].stoneNumber) {
          if (prevBoxToCollect === 0 || prevBoxToCollect === 6) {
            if (
              (nextProps.boxes[prevBoxToCollect].stoneNumber >= 5 && nextProps.boxes[prevBoxToCollect].quanNumber) ||
              nextProps.boxes[prevBoxToCollect].quanNumber === 0
            ) {
              let newBoxCurrentStop = prevBoxToCollect + 1;
              let timeout = undefined;
              clearTimeout(timeout);
              timeout = setTimeout(() => {
                nextProps.collectScore(newBoxCurrentStop, this.state.playerActive, "COUNTER_CLOCKWISE");
              }, this.state.frameCounter);
            } else {
              this.changePlayerActive();
            }
          } else {
            let newBoxCurrentStop = prevBoxToCollect + 1;
            let timeout = undefined;
            clearTimeout(timeout);
            timeout = setTimeout(() => {
              nextProps.collectScore(newBoxCurrentStop, this.state.playerActive, "COUNTER_CLOCKWISE");
            }, this.state.frameCounter);
          }
        } else {
          this.changePlayerActive();
        }
      } else {
        if (
          nextProps.boxCurrentStopId === 2 &&
          nextProps.boxes[0].stoneNumber === 0 &&
          nextProps.boxes[0].quanNumber === 0 &&
          nextProps.boxes[11].stoneNumber > 0
        ) {
          let timeout = undefined;
          clearTimeout(timeout);
          timeout = setTimeout(() => {
            nextProps.collectScore(12, this.state.playerActive, "COUNTER_CLOCKWISE");
          }, this.state.frameCounter);
        } else if (
          nextProps.boxCurrentStopId === 8 &&
          nextProps.boxes[6].stoneNumber === 0 &&
          nextProps.boxes[6].quanNumber === 0 &&
          nextProps.boxes[5].stoneNumber > 0
        ) {
          let timeout = undefined;
          clearTimeout(timeout);
          timeout = setTimeout(() => {
            nextProps.collectScore(6, this.state.playerActive, "COUNTER_CLOCKWISE");
          }, this.state.frameCounter);
        } else {
          this.changePlayerActive();
        }
      }
    }
    //check collect continue
    if (nextProps.boxCurrentCollectId !== this.props.boxCurrentCollectId) {
      if (nextProps.collectDirection === "CLOCKWISE") {
        let nextBoxToCheckNoneStone, nextBoxToCollect;
        if (nextProps.boxCurrentStopId > 11) {
          nextBoxToCheckNoneStone = 0;
        } else {
          nextBoxToCheckNoneStone = nextProps.boxCurrentStopId;
        }
        if (nextProps.boxCurrentStopId + 2 === 13) {
          nextBoxToCollect = 0;
        } else if (nextProps.boxCurrentStopId + 2 > 13) {
          nextBoxToCollect = 1;
        } else {
          nextBoxToCollect = nextProps.boxCurrentStopId + 1;
        }
        if (
          nextProps.boxes[nextBoxToCheckNoneStone].stoneNumber === 0 &&
          nextProps.boxes[nextBoxToCheckNoneStone].quanNumber === 0 &&
          nextProps.boxes[nextBoxToCollect].stoneNumber
        ) {
          if (nextBoxToCollect === 0 || nextBoxToCollect === 6) {
            if (
              (nextProps.boxes[nextBoxToCollect].stoneNumber >= 5 && nextProps.boxes[nextBoxToCollect].quanNumber) ||
              nextProps.boxes[nextBoxToCollect].quanNumber === 0
            ) {
              let timeout = undefined;
              clearTimeout(timeout);
              timeout = setTimeout(() => {
                nextProps.collectScore(nextBoxToCollect + 1, this.state.playerActive, "CLOCKWISE");
              }, this.state.frameCounter);
            } else {
              this.changePlayerActive();
            }
          } else {
            let timeout = undefined;
            clearTimeout(timeout);
            timeout = setTimeout(() => {
              nextProps.collectScore(nextBoxToCollect + 1, this.state.playerActive, "CLOCKWISE");
            }, this.state.frameCounter);
          }
        } else {
          this.changePlayerActive();
        }
      } else {
        let prevBoxToCheckNoneStone, prevBoxToCollect;
        if (nextProps.boxCurrentStopId == 1) {
          prevBoxToCheckNoneStone = 11;
        } else {
          prevBoxToCheckNoneStone = nextProps.boxCurrentStopId - 2;
        }
        if (nextProps.boxCurrentStopId === 2) {
          prevBoxToCollect = 11;
        } else if (nextProps.boxCurrentStopId === 1) {
          prevBoxToCollect = 10;
        } else {
          prevBoxToCollect = nextProps.boxCurrentStopId - 3;
        }
        if (
          nextProps.boxes[prevBoxToCheckNoneStone].stoneNumber === 0 &&
          nextProps.boxes[prevBoxToCheckNoneStone].quanNumber === 0 &&
          nextProps.boxes[prevBoxToCollect].stoneNumber
        ) {
          if (prevBoxToCollect === 0 || prevBoxToCollect === 6) {
            if (
              (nextProps.boxes[prevBoxToCollect].stoneNumber >= 5 && nextProps.boxes[prevBoxToCollect].quanNumber) ||
              nextProps.boxes[prevBoxToCollect].quanNumber === 0
            ) {
              let timeout = undefined;
              clearTimeout(timeout);
              timeout = setTimeout(() => {
                nextProps.collectScore(prevBoxToCollect + 1, this.state.playerActive, "COUNTER_CLOCKWISE");
              }, this.state.frameCounter);
            } else {
              this.changePlayerActive();
            }
          } else {
            let timeout = undefined;
            clearTimeout(timeout);
            timeout = setTimeout(() => {
              nextProps.collectScore(prevBoxToCollect + 1, this.state.playerActive, "COUNTER_CLOCKWISE");
            }, this.state.frameCounter);
          }
        } else {
          this.changePlayerActive();
        }
      }
    }
    //end game or change turn
    if (nextProps.turnPlayer !== this.props.turnPlayer) {
      if (!this.state.endGame) {
        if (this.checkEndGame(nextProps.boxes)) {
          await this.setState({
            endGame: true
          });
          this.props.endGame();
        }
      }
      if (!this.state.endGame) {
        if (this.state.playerActive === 1) {
          this.setState({
            playerActive: 2
          });
          this.props.checkRaiQuan(2);
        } else {
          this.setState({
            playerActive: 1
          });
          this.props.checkRaiQuan(1);
        }
      }
    }
    if (nextProps.botCalculate !== this.props.botCalculate && nextProps.turnPlayer === 1) {
      await this.setState({ botThinking: true });
      let timeoutThinking = undefined;
      clearTimeout(timeoutThinking);
      timeoutThinking = setTimeout(async () => {
        await this.botCalculateScore(nextProps.boxes, true, 1);
        await this.botCalculateRisk();
        await this.botCalculateTotalRiskAndScore(nextProps.boxes);
        await this.botCalculateRiskTurn2();
        await this.botCalculateTotalRiskAndScoreTurn2(nextProps.boxes);
        let newTotalScore = [];
        await this.state.arrayTotalScoreAndRiskBotCalculate.forEach((item, index) => {
          newTotalScore.push({
            ...item,
            score: item.score - this.state.arrayTotalScoreAndRiskBotCalculateTurn2[index].score
          });
        });
        let botChoose = this.minimax(
          0,
          0,
          true,
          newTotalScore,
          { score: -1000 },
          { score: 1000 },
          this.state.arrayTotalScoreAndRiskBotCalculate.length
        );
        await this.setState({ botThinking: false });
        console.log("bot choose: ", botChoose);
        let timeoutBot = undefined;
        clearTimeout(timeoutBot);
        let boxBotChoose = botChoose.boxChoosen;
        if (!nextProps.boxes[boxBotChoose - 1].stoneNumber) {
          for (let i = 2; i < 7; i++) {
            if (nextProps.boxes[i - 1].stoneNumber) {
              boxBotChoose = i;
              break;
            }
          }
        }
        timeoutBot = await setTimeout(async () => {
          await nextProps.changeBoxChose(boxBotChoose);
        }, 1000);
        if (botChoose.direction === "CLOCKWISE") {
          let timeoutBotDivide = undefined;
          clearTimeout(timeoutBotDivide);
          timeoutBotDivide = await setTimeout(async () => {
            await nextProps.divideClockwise(nextProps.boxes[boxBotChoose - 1].stoneNumber);
          }, 1500);
        } else {
          let timeoutBotDivide = undefined;
          clearTimeout(timeoutBotDivide);
          timeoutBotDivide = await setTimeout(async () => {
            await nextProps.divideCounterClockwise(nextProps.boxes[boxBotChoose - 1].stoneNumber);
          }, 1500);
        }
      }, 200);
    }
    if (nextProps.botCalculate !== this.props.botCalculate && nextProps.turnPlayer === 2) {
      this.setState({
        arrayScoreBotCalculate: [],
        arrayRiskBotCalculate: [],
        arrayScoreBotCalculateTurnEnermy: [],
        arrayTotalScoreAndRiskBotCalculate: [],
        arrayScoreBotCalculateTurn2: [],
        arrayScoreBotCalculateTurnEnermyTurn2: [],
        arrayRiskBotCalculateTurn2: [],
        arrayTotalScoreAndRiskBotCalculateTurn2: []
      });
    }
    //end
    if (nextProps.endGameFinish !== this.props.endGameFinish && nextProps.endGameFinish) {
      if (nextProps.scorePlayer1 < nextProps.scorePlayer2) {
        this.setState({
          playerWin: 2
        });
      } else {
        this.setState({
          playerWin: 1
        });
      }
    }
  }

  minimax = (depth, nodeIndex, maximizingPlayer, values, alpha, beta, valueLength) => {
    if (depth === 1) return values[nodeIndex];

    if (maximizingPlayer) {
      let best = { score: -1000 };

      // Recur for left and
      // right children
      for (let i = 0; i < valueLength; i++) {
        let val = this.minimax(depth + 1, nodeIndex * valueLength + i, false, values, alpha, beta);
        if (best.score <= val.score) {
          best = val;
        }
        if (alpha.score <= best.score) {
          alpha = best;
        }

        // Alpha Beta Pruning
        if (beta.score <= alpha.score) break;
      }
      return best;
    } else {
      let best = { score: 1000 };

      // Recur for left and
      // right children
      for (let i = 0; i < 10; i++) {
        let val = this.minimax(depth + 1, nodeIndex * 10 + i, true, values, alpha, beta);
        if (best.score >= val.score) {
          best = val;
        }
        if (beta.score > best.score) {
          beta = best;
        }

        // Alpha Beta Pruning
        if (beta.score <= alpha.score) break;
      }
      return best;
    }
  };

  botCalculateTotalRiskAndScoreTurn2 = async boxes => {
    let newArrayScoreAndRiskBotCalculate = [];
    this.state.arrayScoreBotCalculateTurn2.forEach((objectScore, index) => {
      newArrayScoreAndRiskBotCalculate.push({
        ...objectScore,
        score: objectScore.score - this.state.arrayRiskBotCalculateTurn2[index].score
      });
    });
    let newArrayScoreAndRiskBotCalculateTurn2 = [];
    let maxScoreOfOneTurn2 = newArrayScoreAndRiskBotCalculate[0];
    for (let i = 0; i < newArrayScoreAndRiskBotCalculate.length; i++) {
      if (maxScoreOfOneTurn2.score < newArrayScoreAndRiskBotCalculate[i].score) {
        maxScoreOfOneTurn2 = newArrayScoreAndRiskBotCalculate[i];
      }
      if (i % 100 === 99) {
        newArrayScoreAndRiskBotCalculateTurn2.push(maxScoreOfOneTurn2);
        if (i < 999) {
          maxScoreOfOneTurn2 = newArrayScoreAndRiskBotCalculate[i + 1];
        }
      }
    }
    let finalArrayScoreTurn2 = [];
    newArrayScoreAndRiskBotCalculateTurn2.forEach((item, index) => {
      if (boxes[parseInt(index / 2 + 1)].stoneNumber) {
        finalArrayScoreTurn2.push({
          ...item
        });
      }
    });
    await this.setState({
      arrayTotalScoreAndRiskBotCalculateTurn2: [...finalArrayScoreTurn2]
    });
  };

  botCalculateRiskTurn2 = async () => {
    let maxRiskOfOneTurn = this.state.arrayScoreBotCalculateTurnEnermyTurn2[0].score;
    for (let i = 0; i < this.state.arrayScoreBotCalculateTurnEnermyTurn2.length; i++) {
      if (maxRiskOfOneTurn < this.state.arrayScoreBotCalculateTurnEnermyTurn2[i].score) {
        maxRiskOfOneTurn = this.state.arrayScoreBotCalculateTurnEnermyTurn2[i].score;
      }
      if (i % 10 === 9) {
        await this.setState({
          arrayRiskBotCalculateTurn2: [...this.state.arrayRiskBotCalculateTurn2, { score: maxRiskOfOneTurn }]
        });
        if (i < 9999) {
          maxRiskOfOneTurn = this.state.arrayScoreBotCalculateTurnEnermyTurn2[i + 1].score;
        }
      }
    }
  };

  botCalculateTotalRiskAndScore = async boxes => {
    let newArrayScoreAndRiskBotCalculate = [];
    this.state.arrayScoreBotCalculate.forEach((objectScore, index) => {
      if (boxes[objectScore.boxChoosen - 1].stoneNumber) {
        newArrayScoreAndRiskBotCalculate.push({
          ...objectScore,
          score: objectScore.score - this.state.arrayRiskBotCalculate[index].score
        });
      }
    });
    await this.setState({
      arrayTotalScoreAndRiskBotCalculate: [...newArrayScoreAndRiskBotCalculate]
    });
  };

  botCalculateRisk = async () => {
    let maxRiskOfOneTurn = this.state.arrayScoreBotCalculateTurnEnermy[0].score;
    for (let i = 0; i < this.state.arrayScoreBotCalculateTurnEnermy.length; i++) {
      if (maxRiskOfOneTurn < this.state.arrayScoreBotCalculateTurnEnermy[i].score) {
        maxRiskOfOneTurn = this.state.arrayScoreBotCalculateTurnEnermy[i].score;
      }
      if (i % 10 === 9) {
        await this.setState({
          arrayRiskBotCalculate: [...this.state.arrayRiskBotCalculate, { score: maxRiskOfOneTurn }]
        });
        if (i < 99) {
          maxRiskOfOneTurn = this.state.arrayScoreBotCalculateTurnEnermy[i + 1].score;
        }
      }
    }
  };

  botCalculateScore = async (boxes, isBot, depth, boxChoosen, direction) => {
    if (isBot) {
      for (let i = 2; i <= 6; i++) {
        let clockwiseBoxes = calculateScore(i, "CLOCKWISE", boxes);
        if (depth === 1) {
          await this.setState({
            arrayScoreBotCalculate: [
              ...this.state.arrayScoreBotCalculate,
              {
                score: clockwiseBoxes.score,
                boxChoosen: i,
                direction: "CLOCKWISE"
              }
              // clockwiseBoxes.score
            ]
          });
        }
        if (depth === 3) {
          await this.setState({
            arrayScoreBotCalculateTurn2: [
              ...this.state.arrayScoreBotCalculateTurn2,
              {
                score: clockwiseBoxes.score,
                boxChoosen: i,
                direction: "CLOCKWISE"
              }
              // clockwiseBoxes.score
            ]
          });
        }
        if (depth < 4) {
          await this.botCalculateScore(clockwiseBoxes.boxes, !isBot, depth + 1, i, "CLOCKWISE");
        }
        let counterClockwiseBoxes = calculateScore(i, "COUNTER_CLOCKWISE", boxes);
        if (depth === 1) {
          await this.setState({
            arrayScoreBotCalculate: [
              ...this.state.arrayScoreBotCalculate,
              {
                score: counterClockwiseBoxes.score,
                boxChoosen: i,
                direction: "COUNTER_CLOCKWISE"
              }
              // counterClockwiseBoxes.score
            ]
          });
        }
        if (depth === 3) {
          await this.setState({
            arrayScoreBotCalculateTurn2: [
              ...this.state.arrayScoreBotCalculateTurn2,
              {
                score: counterClockwiseBoxes.score,
                boxChoosen: i,
                direction: "COUNTER_CLOCKWISE"
              }
              // counterClockwiseBoxes.score
            ]
          });
        }
        if (depth < 4) {
          await this.botCalculateScore(counterClockwiseBoxes.boxes, !isBot, depth + 1, i, "COUNTER_CLOCKWISE");
        }
      }
    } else {
      for (let i = 8; i <= 12; i++) {
        let clockwiseBoxes = calculateScore(i, "CLOCKWISE", boxes);
        if (depth === 2) {
          await this.setState({
            arrayScoreBotCalculateTurnEnermy: [
              ...this.state.arrayScoreBotCalculateTurnEnermy,
              {
                score: clockwiseBoxes.score,
                boxChoosen: i,
                direction: "CLOCKWISE"
              }
            ]
          });
        }
        if (depth === 4) {
          await this.setState({
            arrayScoreBotCalculateTurnEnermyTurn2: [
              ...this.state.arrayScoreBotCalculateTurnEnermyTurn2,
              {
                score: clockwiseBoxes.score,
                boxChoosen: i,
                direction: "CLOCKWISE"
              }
            ]
          });
        }
        await this.botCalculateScore(clockwiseBoxes.boxes, !isBot, depth + 1, boxChoosen, direction);
        let counterClockwiseBoxes = calculateScore(i, "COUNTER_CLOCKWISE", boxes);
        if (depth === 2) {
          await this.setState({
            arrayScoreBotCalculateTurnEnermy: [
              ...this.state.arrayScoreBotCalculateTurnEnermy,
              {
                score: counterClockwiseBoxes.score,
                boxChoosen: i,
                direction: "COUNTER_CLOCKWISE"
              }
            ]
          });
        }
        if (depth === 4) {
          await this.setState({
            arrayScoreBotCalculateTurnEnermyTurn2: [
              ...this.state.arrayScoreBotCalculateTurnEnermyTurn2,
              {
                score: counterClockwiseBoxes.score,
                boxChoosen: i,
                direction: "COUNTER_CLOCKWISE"
              }
            ]
          });
        }
        await this.botCalculateScore(counterClockwiseBoxes.boxes, !isBot, depth + 1, boxChoosen, direction);
      }
    }
  };

  formatBoxes = boxes => {
    let leftFormatBoxes = boxes.filter(box => {
      if (box.id > 7) {
        return false;
      }
      return true;
    });
    let rightFormatBoxes = boxes.filter(box => {
      if (box.id > 7) {
        return true;
      }
      return false;
    });
    let newFormatboxes = [...leftFormatBoxes, ...rightFormatBoxes.reverse()];
    this.setState({
      boxes: newFormatboxes
    });
  };

  changePlayerActive = () => {
    if (!this.state.endGame) {
      if (this.state.playerActive === 1) {
        this.props.changeTurnPlayer(2);
      } else {
        this.props.changeTurnPlayer(1);
      }
    }
  };

  checkEndGame = boxes => {
    if (!boxes[0].stoneNumber && !boxes[0].quanNumber && !boxes[6].stoneNumber && !boxes[6].quanNumber) {
      return true;
    }
    return false;
  };

  render() {
    return (
      <div className="o_an_quan" style={{ backgroundImage: `url(${bigBackground})` }}>
        <div className="main_game">
          <div className="map">
            <img src={background}></img>
          </div>
          {this.state.botThinking ? <h4 className="bot_thinking">Đợi tớ nghĩ cái...</h4> : null}
          <h4 className="score_1">
            {this.props.scorePlayer1} điểm
            <div>
              {this.state.playerWin === 1 ? (
                <h4>Đồ con gà! Tuổi?</h4>
              ) : this.state.playerWin === 2 ? (
                <h4>Lag</h4>
              ) : null}
            </div>
          </h4>
          <div className={classnames({ player_hidden: this.state.playerActive !== 1 }, "player1")}>
            <h3>Chơi</h3>
          </div>
          {this.state.boxes.length ? (
            <div className="box_render">
              <div className="box_quan">
                <Box boxInfo={this.state.boxes[0]}></Box>
              </div>
              <div className="box_center row">
                {this.state.boxes.map(box => {
                  if (box.id !== 1 && box.id !== 7) {
                    return (
                      <div className={classnames({ box_under: Number(box.id) >= 8 }, "col-2")} key={box.id}>
                        <Box boxInfo={box} player={this.state.playerActive}></Box>
                      </div>
                    );
                  }
                })}
              </div>
              <div className="box_quan">
                <Box boxInfo={this.state.boxes[6]}></Box>
              </div>
            </div>
          ) : null}
          <div className={classnames({ player_hidden: this.state.playerActive !== 2 }, "player2")}>
            <h3>Chơi</h3>
          </div>
          <h4 className="score_2">
            {this.props.scorePlayer2} điểm
            <div>{this.state.playerWin === 2 ? <h4>Win</h4> : null}</div>
          </h4>
          {this.state.playerWin ? (
            <Link to="/">
              <h4 className="play_again">
                <button>Chơi lại</button>
              </h4>
            </Link>
          ) : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    boxes: state.boxes.boxesInfo,
    stoneNumberNeedToDivide: state.boxes.stoneNumberNeedToDivide,
    boxCurrentStopId: state.boxes.boxCurrentStopId,
    stoneNumberNeedToDivideCounter: state.boxes.stoneNumberNeedToDivideCounter,
    scorePlayer1: state.boxes.scorePlayer1,
    scorePlayer2: state.boxes.scorePlayer2,
    boxCurrentCollectId: state.boxes.boxCurrentCollectId,
    collectDirection: state.boxes.collectDirection,
    turnPlayer: state.boxes.turnPlayer,
    botCalculate: state.boxes.botCalculate,
    endGameFinish: state.boxes.endGame
  };
};

const mapDispatchToProps = dispatch => {
  return {
    stepDivideClockwise: () => {
      dispatch({ type: "STEP_DIVIDE_CLOCKWISE" });
    },
    changeBoxChose: boxId => {
      dispatch({ type: "CHANGE_BOX_CHOSE", data: boxId });
    },
    divideClockwise: stoneNumber => {
      dispatch({ type: "DIVIDE_CLOCKWISE", data: stoneNumber });
    },
    collectScore: (stopBox, player, direction) => {
      dispatch({ type: "COLLECT_SCORE", data: { stopBox: stopBox, player: player, direction: direction } });
    },
    divideCounterClockwise: stoneNumber => {
      dispatch({ type: "DIVIDE_COUNTER_CLOCKWISE", data: stoneNumber });
    },
    stepDivideCounterClockwise: () => {
      dispatch({ type: "STEP_DIVIDE_COUNTER_CLOCKWISE" });
    },
    checkRaiQuan: player => {
      dispatch({ type: "CHECK_RAI_QUAN", data: player });
    },
    endGame: () => {
      dispatch({ type: "END_GAME" });
    },
    changeTurnPlayer: player => {
      dispatch({ type: "CHANGE_TURN_PLAYER", data: player });
    },
    resetBoxes: () => {
      dispatch({ type: "RESET_BOXES" });
    }
    // changeTurnPlayerBot: player => {
    //   dispatch({ type: 'CHANGE_TURN_PLAYER_BOT', data: player });
    // }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainGameBot);

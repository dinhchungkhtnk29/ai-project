import React, { Component } from "react";
import "./Box.scss";
import Stone from "../Stone/Stone";
import Quan from "../Quan/Quan";
import { connect } from "react-redux";
import classnames from "classnames";
import box1 from "../../../img/1.png";
import box2 from "../../../img/2.png";
import box3 from "../../../img/3.png";
import box4 from "../../../img/4.png";
import box5 from "../../../img/5.png";
import box6 from "../../../img/6.png";
import box7 from "../../../img/7.png";
import quan from "../../../img/quan.png";
import LeftButton from "../../../img/right.png";
import RightButton from "../../../img/left.png";

class Box extends Component {
  showStone = stoneNumber => {
    if (stoneNumber === 1) {
      return (
        <div>
          <img className="stone_img" src={box1}></img>
        </div>
      );
    } else if (stoneNumber === 2) {
      return (
        <div>
          <img className="stone_img" src={box2}></img>
        </div>
      );
    } else if (stoneNumber === 3) {
      return (
        <div>
          <img className="stone_img" src={box3}></img>
        </div>
      );
    } else if (stoneNumber === 4) {
      return (
        <div>
          <img className="stone_img" src={box4}></img>
        </div>
      );
    } else if (stoneNumber === 5) {
      return (
        <div>
          <img className="stone_img" src={box5}></img>
        </div>
      );
    } else if (stoneNumber === 6) {
      return (
        <div>
          <img className="stone_img" src={box6}></img>
        </div>
      );
    } else if (stoneNumber >= 7) {
      return (
        <div>
          <img className="stone_img" src={box7}></img>
        </div>
      );
    } else {
      return <div></div>;
    }
  };

  showQuan = quanNumber => {
    if (quanNumber) {
      return (
        <div className="quan_img_render">
          <img className="quan_img" src={quan}></img>
        </div>
      );
    }
    return null;
  };

  handleClickBox = boxInfo => {
    this.props.changeBoxChose(boxInfo.id);
  };

  handleClickArrowCounterClockwise = (boxId, stoneNumber) => {
    if (stoneNumber) {
      if (boxId < 7) {
        this.props.divideCounterClockwise(stoneNumber);
      } else {
        this.props.divideClockwise(stoneNumber);
      }
    }
  };

  handleClickArrowClockwise = (boxId, stoneNumber) => {
    if (stoneNumber) {
      if (boxId < 7) {
        this.props.divideClockwise(stoneNumber);
      } else {
        this.props.divideCounterClockwise(stoneNumber);
      }
    }
  };

  render() {
    return (
      <div
        className="box"
        style={
          this.props.boxInfo.id === 1 || this.props.boxInfo.id === 7
            ? this.props.right
              ? { height: "300px", paddingTop: "120px", paddingLeft: "52px" }
              : { height: "300px", paddingTop: "120px" }
            : null
        }
      >
        {this.props.boxInfo.id != 1 && this.props.boxInfo.id != 7 ? (
          (this.props.boxInfo.id > 1 && this.props.boxInfo.id < 7 && this.props.player === 1) ||
          (this.props.boxInfo.id > 7 && this.props.player === 2) ? (
            // <i
            //   className={classnames(
            //     { show: this.props.boxInfo.id === this.props.boxChosen },
            //     "fas fa-chevron-double-left"
            //   )}
            //   onClick={() => {
            //     this.handleClickArrowCounterClockwise(this.props.boxInfo.id, this.props.boxInfo.stoneNumber);
            //   }}
            // ></i>
            <img
              src={LeftButton}
              className={classnames(
                { show: this.props.boxInfo.id === this.props.boxChosen },
                "fas fa-chevron-double-left left_button"
              )}
              onClick={() => {
                this.handleClickArrowCounterClockwise(this.props.boxInfo.id, this.props.boxInfo.stoneNumber);
              }}
            ></img>
          ) : null
        ) : null}{" "}
        <div
          onClick={() => {
            this.handleClickBox(this.props.boxInfo);
          }}
          className={classnames({ box_quan_item: this.props.boxInfo.quanNumber })}
        >
          {this.showStone(this.props.boxInfo.stoneNumber)}
          {this.showQuan(this.props.boxInfo.quanNumber)}
        </div>
        {this.props.boxInfo.id != 1 && this.props.boxInfo.id != 7 ? (
          (this.props.boxInfo.id > 1 && this.props.boxInfo.id < 7 && this.props.player === 1) ||
          (this.props.boxInfo.id > 7 && this.props.player === 2) ? (
            <img
              className={classnames(
                { show: this.props.boxInfo.id === this.props.boxChosen },
                "fas fa-chevron-double-right right_button"
              )}
              onClick={() => {
                this.handleClickArrowClockwise(this.props.boxInfo.id, this.props.boxInfo.stoneNumber);
              }}
              src={RightButton}
            ></img>
          ) : null
        ) : null}
        <div className={classnames({ number_stone_quan: this.props.boxInfo.quanNumber }, "number_stone")}>
          {this.props.boxInfo.stoneNumber * 1 + this.props.boxInfo.quanNumber * 5}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    boxChosen: state.boxes.boxChosen,
    stoneNumberNeedToDivide: state.boxes.stoneNumberNeedToDivide
  };
};

const mapDispathToProps = dispatch => {
  return {
    changeBoxChose: boxId => {
      dispatch({ type: "CHANGE_BOX_CHOSE", data: boxId });
    },
    divideClockwise: stoneNumber => {
      dispatch({ type: "DIVIDE_CLOCKWISE", data: stoneNumber });
    },
    divideCounterClockwise: stoneNumber => {
      dispatch({ type: "DIVIDE_COUNTER_CLOCKWISE", data: stoneNumber });
    }
  };
};

export default connect(mapStateToProps, mapDispathToProps)(Box);
